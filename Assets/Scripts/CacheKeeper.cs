﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public static class CacheKeeper {


    public static Dictionary<string, Content> cache;

    public static Dictionary<string, Content> LoadCacheFile()
    {
        Debug.Log("Load Cache file");
        Dictionary<string, Content> cache;
        if (File.Exists(Application.persistentDataPath + "/cacheData.dat"))
        {
            Debug.Log("Cache file exists");

            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/cacheData.dat", FileMode.Open);
            cache = (Dictionary<string, Content>) bf.Deserialize(file);
            file.Close();
        }
        else
        {
            Debug.Log("Cache file not exists");
            cache = new Dictionary<string, Content>();
        }
        Debug.Log("cache size - "+cache.Count);
        return cache;
    }

    public static void SaveCacheFile()
    {
        Debug.Log("Save Cache file");
        Debug.Log("cache size - " + cache.Count);
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/cacheData.dat"); //you can call it anything you want
        bf.Serialize(file, cache);
        file.Close();
    }
}

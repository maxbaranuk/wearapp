﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Xml;
using UnityEngine.UI;

public class MainMenuScript : MonoBehaviour
{
    [SerializeField]
    GameObject tutorialPanel;

    string path = "";
    TextAsset GameAsset;
    //public Text startBut;
    public Text manualBut;
    public Text downloadBut;
    public Text feedbackBut;

    private AsyncOperation async = null;
    private Image Empty;
    private RectTransform Full;

    void Start ()
    {
        Screen.orientation =  ScreenOrientation.Portrait;
        switch (Application.systemLanguage)
        {
            case SystemLanguage.English:
                path = "XML/stringsEn";
                break;
            case SystemLanguage.Russian:
                path = "XML/stringsRu";
                break;
            case SystemLanguage.Ukrainian:
                path = "XML/stringsUa";
                break;
            default:
                path = "XML/stringsEn";
                break;
        }
        SetLanguage(path); 
    }


    void SetLanguage(string path)
    {
        XmlDocument xmlDoc = new XmlDocument();
        GameAsset = Resources.Load<TextAsset>(path);
        xmlDoc.LoadXml(GameAsset.text);
        XmlNodeList levelsList = xmlDoc.GetElementsByTagName("Language");
        XmlNodeList levelcontent = levelsList[0].ChildNodes;

        foreach (XmlNode levelInfo in levelcontent)
        {
            //if (levelInfo.Name == "start")
                //startBut.text = "" + levelInfo.InnerText;
            if (levelInfo.Name == "manual")
                manualBut.text = "" + levelInfo.InnerText;
            else if (levelInfo.Name == "download")
                downloadBut.text = "" + levelInfo.InnerText;
            else if (levelInfo.Name == "feedback")
                feedbackBut.text = "" + levelInfo.InnerText;
        }
    }

    void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
	}

    public void StartAR()
    {
        //string expPath = GooglePlayDownloader.GetExpansionFilePath();
        //string mainPath = GooglePlayDownloader.GetMainOBBPath(expPath);
        //string patchPath = GooglePlayDownloader.GetPatchOBBPath(expPath);
        //if (mainPath == null || patchPath == null)
        //    GooglePlayDownloader.FetchOBB();
        //else
        GameObject go = Instantiate(Resources.Load<GameObject>("Prefabs/CircleLoading"));
        RectTransform goRecdt = go.GetComponent<RectTransform>();
        goRecdt.SetParent(FindObjectOfType<Canvas>().GetComponent<RectTransform>());
        goRecdt.offsetMax = Vector2.zero;
        goRecdt.offsetMin = Vector2.zero;
        goRecdt.localScale = Vector3.one;
        goRecdt.localEulerAngles = Vector3.zero;


        Empty = go.transform.GetChild(1).FindChild("Empty").GetComponent<Image>();
        Full = go.transform.GetChild(1).FindChild("Full").GetComponent<RectTransform>();

        async = SceneManager.LoadSceneAsync(2);
        StartCoroutine(ProgressBar(Full.localPosition.x));
    }

    IEnumerator ProgressBar(float FullLocalPosX)
    {
        while (async.progress < 1)
        {
            float increment = async.progress * 500;
            Full.localPosition = new Vector3(FullLocalPosX + increment / 2, Full.localPosition.y, Full.localPosition.z);
            Full.sizeDelta = new Vector2(increment, Full.sizeDelta.y);
            yield return null;
        }
    }

    public void ManualButtonPressed()
    {
        tutorialPanel.SetActive(true);
    }

    public void DownloadButtonPressed()
    {
        Application.OpenURL("https://drive.google.com/folderview?id=0B4JLV0NgSFsERHFyR0lWcUlhTzA&usp=drive_web");
    }

    public void FeedbackButtonPressed()
    {
        sendMail();
    }

    public void sendMail()
    {

        //email Id to send the mail to
        string email = "info.wearstudio@wear-studio.com";
        //subject of the mail
        string subject = MyEscapeURL("Support");
        //body of the mail which consists of Device Model and its Operating System
        string body = MyEscapeURL("Please Enter your message here\n\n\n\n" +
         "________" +
         "\n\nSend from\n\n" +
         "Model: " + SystemInfo.deviceModel + "\n\n" +
            "OS: " + SystemInfo.operatingSystem + "\n\n");

        //Open the Default Mail App
 //       Application.OpenURL("mailto:" + email + "?subject=" + subject + "&body=" + body);

#if UNITY_IOS
		Application.OpenURL("mailto:" + email + "?subject=" + subject
			+ "&body=" + body);
#else
        Application.OpenURL("mailto:" + email + "?subject=" + subject
                    + "&body=" + body);
#endif
    }

    string MyEscapeURL(string url)
    {
        return WWW.EscapeURL(url).Replace("+", "%20");
    }
}

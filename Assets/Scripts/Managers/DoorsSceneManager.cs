﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class DoorsSceneManager : MonoBehaviour
{

    public GameObject[] doors;
    public GameObject[] divans;
    public GameObject[] chairs;
    public GameObject[] tables;
    public GameObject[] others;

    public Material[] doorMaterials;
    public Material[] tableMaterials;

    [SerializeField]
    Material[] chair1Materials;
    [SerializeField]
    Material[] chair2Materials;
    [SerializeField]
    Material[] chair3Materials;
    [SerializeField]
    Material[] sofa1Materials;
    [SerializeField]
    Material[] sofa2Materials;
    [SerializeField]
    Material[] sofa3Materials;
    Material[] currMaterials;
    Transform ui;
    public Material[] firePlaceMaterials;

    public GameObject[] currentMode;
    public int currentItem = 0;
    int currMatIndex = 0;
    int currSofaMatIndex = 0;
    int currTableMatIndex = 0;
    int currMarmourMatIndex = 0;

    void Awake()
    {
        ui = GameObject.Find("Canvas").transform.FindChild("DoorsPanel");
        UpdateShaders();
        foreach (GameObject g in doors) g.tag = "Door";
        foreach (GameObject g in divans) g.tag = "Sofa";
        foreach (GameObject g in chairs) g.tag = "Sofa";
        foreach (GameObject g in tables) g.tag = "Table";
        others[0].tag = "FirePlace";
    }

    void OnEnable() {
        ui.GetComponent<RectTransform>().localScale = Vector3.one;
       
        UIManager.ModeButtonClick += ModeSelect;
        UIManager.NextButtonClick += Next;
        UIManager.PreviousButtonClick += Prev;
        UIManager.tapObject += Tap;
    }


    
    void Start ()
    {
        currentMode = chairs;
        currentMode[currentItem].SetActive(true);
    }

    void Update ()
    {
//        UserInput();
    }

    void OnDisable() {
        UIManager.ModeButtonClick -= ModeSelect;
        UIManager.NextButtonClick -= Next;
        UIManager.PreviousButtonClick -= Prev;
        UIManager.tapObject -= Tap;
        ui.GetComponent<RectTransform>().localScale = Vector3.zero;
    }

    void UpdateShaders()
    {       
        foreach (Material m in doorMaterials)
        {
            var shaderName = m.shader.name;
            var newShader = Shader.Find(shaderName);
            if (newShader != null) m.shader = newShader;
        }
        foreach (Material m in tableMaterials)
        {
            var shaderName = m.shader.name;
            var newShader = Shader.Find(shaderName);
            if (newShader != null) m.shader = newShader;
        }
        foreach (Material m in chair1Materials)
        {
            var shaderName = m.shader.name;
            var newShader = Shader.Find(shaderName);
            if (newShader != null) m.shader = newShader;
        }
        foreach (Material m in chair2Materials)
        {
            var shaderName = m.shader.name;
            var newShader = Shader.Find(shaderName);
            if (newShader != null) m.shader = newShader;
        }
        foreach (Material m in chair3Materials)
        {
            var shaderName = m.shader.name;
            var newShader = Shader.Find(shaderName);
            if (newShader != null) m.shader = newShader;
        }
        foreach (Material m in sofa1Materials)
        {
            var shaderName = m.shader.name;
            var newShader = Shader.Find(shaderName);
            if (newShader != null) m.shader = newShader;
        }
        foreach (Material m in sofa2Materials)
        {
            var shaderName = m.shader.name;
            var newShader = Shader.Find(shaderName);
            if (newShader != null) m.shader = newShader;
        }
        foreach (Material m in sofa3Materials)
        {
            var shaderName = m.shader.name;
            var newShader = Shader.Find(shaderName);
            if (newShader != null) m.shader = newShader;
        }
        foreach (Material m in firePlaceMaterials)
        {
            var shaderName = m.shader.name;
            var newShader = Shader.Find(shaderName);
            if (newShader != null) m.shader = newShader;
        }
    }

    public void Prev()
    {        
            currentMode[currentItem].SetActive(false);
            currentItem--;
            if (currentItem < 0) currentItem = currentMode.Length - 1;
            currentMode[currentItem].SetActive(true);
    }

    public void Next()
    {
            currentMode[currentItem].SetActive(false);
            currentItem++;
            if (currentItem == currentMode.Length) currentItem = 0;
            currentMode[currentItem].SetActive(true);
    }

    public void ChangeDoorMaterial(GameObject g)
    {
        g.GetComponent<MeshRenderer>().material = doorMaterials[currMatIndex];
        currMatIndex++;
        if (currMatIndex == doorMaterials.Length)
            currMatIndex -= doorMaterials.Length;
    }

    public void ChangeTableMaterial(GameObject g)
    {
        g.GetComponent<MeshRenderer>().material = tableMaterials[currTableMatIndex];
        currTableMatIndex++;
        if (currTableMatIndex == tableMaterials.Length)
            currTableMatIndex -= tableMaterials.Length;
    }

    public void ChangeSofaMaterial(GameObject g)
    {
        switch (g.transform.parent.name)
        {
            case "sofa_1":
                currMaterials = sofa1Materials;
                break;
            case "sofa_2":
                currMaterials = sofa2Materials;
                break;
            case "sofa_3":
                currMaterials = sofa3Materials;
                break;
            case "armchair_1":
                currMaterials = chair1Materials;
                break;
            case "armchair_2":
                currMaterials = chair2Materials;
                break;
            case "armchair_3":
                currMaterials = chair3Materials;
                break;
        }
        g.GetComponent<MeshRenderer>().material = currMaterials[currSofaMatIndex];
        currSofaMatIndex++;
        if (currSofaMatIndex == currMaterials.Length)
            currSofaMatIndex -= currMaterials.Length;
    }

    public void ChangeFirePlaceMaterial(GameObject g)
    {
        g.GetComponent<MeshRenderer>().material = firePlaceMaterials[currMarmourMatIndex];
        currMarmourMatIndex++;
        if (currMarmourMatIndex == firePlaceMaterials.Length)
            currMarmourMatIndex -= firePlaceMaterials.Length;
    }

    public void ModeSelect(string mode) {
        currentMode[currentItem].SetActive(false);
        currentItem = 0;
        switch (mode) {
            case "Chair":
                currentMode = chairs;
                break;
            case "Doors":
                currentMode = doors;
                break;
            case "Sofas":
                currentMode = divans;
                break;
            case "Tables":
                currentMode = tables;
                break;
            case "Other":
                currentMode = others;
                break;
        }
        currentMode[currentItem].SetActive(true);
    }

    void Tap(Transform tr) {
        if (tr.tag == "Door")
            ChangeDoorMaterial(tr.gameObject);
        else if (tr.tag == "Table")
            ChangeTableMaterial(tr.gameObject);
        else if (tr.tag == "Sofa")
            ChangeSofaMaterial(tr.gameObject);
        else if (tr.tag == "FirePlace")
            ChangeFirePlaceMaterial(tr.gameObject);
    }

    void UserInput()
    {
        if (Application.isEditor)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, 100) && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
                {
                    Transform hitTr = hit.transform;
                    if (hitTr.tag == "Door")
                        ChangeDoorMaterial(hitTr.gameObject);
                    else if (hitTr.tag == "Table")
                        ChangeTableMaterial(hitTr.gameObject);                   
                    else if (hitTr.tag == "Sofa")
                        ChangeSofaMaterial(hitTr.gameObject);
                    else if (hitTr.tag == "FirePlace")
                        ChangeFirePlaceMaterial(hitTr.gameObject);
                }
            }
        }
        else
        {
            if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, 100) && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
                {
                    Transform hitTr = hit.transform;
                    if (hitTr.tag == "Door")
                        ChangeDoorMaterial(hitTr.gameObject);                 
                    if (hitTr.tag == "Table")
                        ChangeTableMaterial(hitTr.gameObject);              
                    if (hitTr.tag == "Sofa")
                        ChangeSofaMaterial(hitTr.gameObject);
                    if (hitTr.tag == "FirePlace")
                        ChangeFirePlaceMaterial(hitTr.gameObject);
                }
            }
        }
    }
}

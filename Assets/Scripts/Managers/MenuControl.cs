﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuControl : MonoBehaviour {

//    public delegate void MenuClick();
//    public static MenuClick OpenTutorial;
    GameObject[] buttons;
    bool process;
    bool open;
    public Sprite openedMenuImage;
    public Sprite closedMenuImage;
    public GameObject tutorialPanel;
    void Awake() {
        buttons = new GameObject[transform.childCount];
        for (int i = 0; i < buttons.Length; i++) {
            buttons[i] = transform.GetChild(i).gameObject;
        }

        for (int i = 0; i<buttons.Length-1; i++) {
            buttons[i].SetActive(false);
        }
    }

    public void MenuSwitcher() {
        if (!process) {
            if (open) CloseMenu();
            else OpenMenu();
        }
    }

	public void OpenMenu () {
        StartCoroutine(Open());
	}

    public void CloseMenu()
    {
        StartCoroutine(Close());
    }

    IEnumerator Open() {
        process = true;
        
        for (int i = buttons.Length - 2; i >= 0; i--) {
            buttons[i].SetActive(true);
            yield return new WaitForSeconds(0.1f);
        }
        process = false;
        open = true;
        buttons[buttons.Length - 1].GetComponentInChildren<Image>().sprite = openedMenuImage;
    }

    IEnumerator Close()
    {
        process = true;
        for (int i = 0; i < buttons.Length - 1; i++)
        {
            buttons[i].SetActive(false);
            yield return new WaitForSeconds(0.1f);
        }
        process = false;
        open = false;
        buttons[buttons.Length - 1].GetComponentInChildren<Image>().sprite = closedMenuImage;
    }

    //public void ManualButtonPressed()
    //{
    //    OpenTutorial();    
    //}

    public void DownloadButtonPressed()
    {
        CloseMenu();
        Application.OpenURL("https://drive.google.com/folderview?id=0B4JLV0NgSFsERHFyR0lWcUlhTzA&usp=drive_web");
    }

    public void FeedbackButtonPressed()
    {
        CloseMenu();
        sendMail();
    }

    public void sendMail()
    {
        
        //email Id to send the mail to
        string email = "info.wearstudio@wear-studio.com";
        //subject of the mail
        string subject = MyEscapeURL("Support");
        //body of the mail which consists of Device Model and its Operating System
        string body = MyEscapeURL("Please Enter your message here\n\n\n\n" +
         "________" +
         "\n\nSend from\n\n" +
         "Model: " + SystemInfo.deviceModel + "\n\n" +
            "OS: " + SystemInfo.operatingSystem + "\n\n");

       
#if UNITY_IOS
		Application.OpenURL("mailto:" + email + "?subject=" + subject
			+ "&body=" + body);
#else
        Application.OpenURL("mailto:" + email + "?subject=" + subject
                    + "&body=" + body);
#endif
    }

    string MyEscapeURL(string url)
    {
        return WWW.EscapeURL(url).Replace("+", "%20");
    }
}

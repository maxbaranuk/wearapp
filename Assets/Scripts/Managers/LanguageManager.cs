﻿using UnityEngine;
using UnityEngine.UI;
using System.Xml;

public class LanguageManager : MonoBehaviour
{

    string path = "";
    TextAsset GameAsset;
    public Text targetText;
    public Text tableText;
    public Text doorsText;
    public Text divansText;
    public Text chairText;
    public Text otherText;
    public Text infoText;
    public Text doorsInfo1Text;
    public Text doorsInfo2Text;
    public Text doorsInfo3Text;

    public Text houseInfoScale;
    public Text houseInfoRotate;
    //public Text brickText;
    //public Text tileText;
    //public Text doorColorText;
    //public Text doorTextureText;
    //public Text handsColorText;
    //public Text windowFrameText;
    //public Text concreteTextureText;
    //public Text exitText;

    public Text minionfingersText;
    public Text minionSpeakText;
    public Text minionHeadsText;
    public Text minionFeaturessText;

    //public Text floorInfo1Text;
    //public Text floorInfo2Text;

    void Start ()
    {
        switch (Application.systemLanguage)
        {
            case SystemLanguage.English:
                path = "XML/stringsEn";
                break;
            case SystemLanguage.Russian:
                path = "XML/stringsRu";
                break;
            case SystemLanguage.Ukrainian:
                path = "XML/stringsUa";
                break;
            default:
                path = "XML/stringsEn";
                break;
        }
        SetLanguage(path);
    }

    private void SetLanguage(string path)
    {
        XmlDocument xmlDoc = new XmlDocument();
        GameAsset = Resources.Load<TextAsset>(path);
        xmlDoc.LoadXml(GameAsset.text);
        XmlNodeList levelsList = xmlDoc.GetElementsByTagName("Language");
        XmlNodeList levelcontent = levelsList[0].ChildNodes;

        foreach (XmlNode levelInfo in levelcontent)
        {

            if (levelInfo.Name == "target")
                targetText.text = "" + levelInfo.InnerText;
            else if (levelInfo.Name == "chairs")
                chairText.text = "" + levelInfo.InnerText;
            else if (levelInfo.Name == "doors")
                doorsText.text = "" + levelInfo.InnerText;
            else if (levelInfo.Name == "divans")
                divansText.text = "" + levelInfo.InnerText;
            else if (levelInfo.Name == "tables")
                tableText.text = "" + levelInfo.InnerText;
            else if (levelInfo.Name == "other")
                otherText.text = "" + levelInfo.InnerText;
            else if (levelInfo.Name == "info")
                infoText.text = "" + levelInfo.InnerText;
            else if (levelInfo.Name == "changeColor")
                doorsInfo1Text.text = "" + levelInfo.InnerText;
            else if (levelInfo.Name == "rotate")
                doorsInfo2Text.text = "" + levelInfo.InnerText;
            else if (levelInfo.Name == "move")
                doorsInfo3Text.text = "" + levelInfo.InnerText;
            else if (levelInfo.Name == "houseScale")
                houseInfoScale.text = "" + levelInfo.InnerText;
            else if (levelInfo.Name == "houserotate")
                houseInfoRotate.text = "" + levelInfo.InnerText;
            else if (levelInfo.Name == "fingers")
                minionfingersText.text = "" + levelInfo.InnerText;
            else if (levelInfo.Name == "minionSpeak")
                minionSpeakText.text = "" + levelInfo.InnerText;
            else if (levelInfo.Name == "minionHeads")
                minionHeadsText.text = "" + levelInfo.InnerText;
            else if (levelInfo.Name == "minionPropeties")
                minionFeaturessText.text = "" + levelInfo.InnerText;

            //else if (levelInfo.Name == "floorTap")
            //    floorInfo1Text.text = "" + levelInfo.InnerText;
            //else if (levelInfo.Name == "floorChanger")
            //    floorInfo2Text.text = "" + levelInfo.InnerText;
            //if (levelInfo.Name == "doorColor") doorColorText.text = "" + levelInfo.InnerText;
            //if (levelInfo.Name == "doorTexture") doorTextureText.text = "" + levelInfo.InnerText;
            //if (levelInfo.Name == "handsColor") handsColorText.text = "" + levelInfo.InnerText;
            //if (levelInfo.Name == "windowFrame") windowFrameText.text = "" + levelInfo.InnerText;
            //if (levelInfo.Name == "concreteTexture") concreteTextureText.text = "" + levelInfo.InnerText;
            //if (levelInfo.Name == "exit") exitText.text = "" + levelInfo.InnerText;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    public delegate void  UIAction();
    public delegate void UIModeAction(string mode);
    public delegate void TapAction(Transform tr);
    public static UIAction GameButtonClick;
    public static UIAction ShopButtonClick;
    public static UIAction InfoButtonClick;
    public static UIAction MinionClick;
    public static UIAction MinionVideoButtonClick;
    public static UIModeAction ModeButtonClick;
    public static UIAction NextButtonClick;
    public static UIAction PreviousButtonClick;
    public static TapAction tapObject;
//    public Text info;
    public GameObject loading;
    Image fillProgress;
    public GameObject creating;
    public Text errorPanel;
    public GameObject tutorial;
    List<GameObject> activeCanvasObjects;
    public Button gameButton;
    public Button infoButton;
    public Button shopButton;
    public GameObject score;

    void OnEnable () {
        infoButton.interactable = false;
        fillProgress = loading.transform.FindChild("Fill").GetComponent<Image>();
        ContentLoader.LoadStart += StartLoad;
        ContentLoader.LoadFinishError += LoadError;
        ContentLoader.LoadFinishOk += LoadComplete;
        ContentLoader.showProgress += ShowProgress;
        ContentCreator.message += Message;
        ContentCreator.createStart += CreateStart;
        ContentCreator.createFinish += Createfinish;
        ContentCreator.error += LoadError;
//        MenuControl.OpenTutorial += ShowTutorial;
    }

    void Update() {
        UserInput();
    }
	// Update is called once per frame
	void OnDisable () {
        ContentLoader.LoadStart -= StartLoad;
        ContentLoader.LoadFinishError -= LoadError;
        ContentLoader.LoadFinishOk -= LoadComplete;
        ContentLoader.showProgress -= ShowProgress;
        ContentCreator.message -= Message;
        ContentCreator.createStart -= CreateStart;
        ContentCreator.createFinish -= Createfinish;
        ContentCreator.error -= LoadError;
//        MenuControl.OpenTutorial -= ShowTutorial;
    }

    void CreateStart(string text) {
        creating.SetActive(true);
    }

    void Createfinish(string text)
    {
        creating.SetActive(false);
    }

    void Message(string text)
    {
        Debug.Log(text);
  //      info.text = text;
    }
    void StartLoad(string text) {
//        info.text += "Loading start...\n";
        Debug.Log("Loading start...");
        fillProgress.fillAmount = 0;
        loading.SetActive(true);
    }

    void LoadError(string text) {
        Debug.Log("Error!!!   " + text);
        StartCoroutine(ShowError(text));
        loading.SetActive(false);
        creating.SetActive(false);
    }

    IEnumerator ShowError(string text) {
        errorPanel.text = "Error "+text;
        errorPanel.gameObject.SetActive(true);
        yield return new WaitForSeconds(3);
//        errorPanel.text = "";
        errorPanel.gameObject.SetActive(false);
    }

    void LoadComplete(string id)
    {
//        info.text += "Load complete!!!\n";
        Debug.Log("Load complete!!!");
        loading.SetActive(false);
    }

    void ShowProgress(float progress) {
        fillProgress.fillAmount = progress;
    }

    public void ShowTutorial() {
        activeCanvasObjects = new List<GameObject>();
        GameObject canv = GameObject.Find("Canvas");
        int itemsCount = canv.transform.childCount;
        for (int i = itemsCount - 2; i > 0; i--)
        {
            Transform t = canv.transform.GetChild(i);
            if (t.gameObject.activeInHierarchy)
                activeCanvasObjects.Add(canv.transform.GetChild(i).gameObject);
        }
        Debug.Log(activeCanvasObjects.Count);
        foreach (GameObject g in activeCanvasObjects) g.SetActive(false);
        tutorial.SetActive(true);
    }

    public void CloseTutorial() {
        foreach (GameObject g in activeCanvasObjects) g.SetActive(true);
        activeCanvasObjects.Clear();
        tutorial.SetActive(false);
    }

    public void GameButton() {
        gameButton.interactable = false;
        infoButton.interactable = true;
        shopButton.interactable = true;
        score.SetActive(true);
        GameButtonClick();
    }
    public void ShopButton()
    {
        gameButton.interactable = true;
        infoButton.interactable = true;
        shopButton.interactable = false;
        score.SetActive(false);
        ShopButtonClick();
    }
    public void InfoButton()
    {
        gameButton.interactable = true;
        infoButton.interactable = false;
        shopButton.interactable = true;
        score.SetActive(false);
        InfoButtonClick();
    }

    public void ModeButton(string mode) {
        ModeButtonClick(mode);
    }

    public void NextButton()
    {
        NextButtonClick();
    }
    public void PrevButton()
    {
        PreviousButtonClick();
    }

    public void VideoButton()
    {
        MinionVideoButtonClick();
    }


    public void UserInput()
    {
        if (!Application.isEditor)
        {
			if (Input.touchCount > 0) {
				if (Input.GetTouch (0).phase == TouchPhase.Began) {
					Ray ray = Camera.main.ScreenPointToRay (Input.GetTouch (0).position);
					RaycastHit hit;

					if (Physics.Raycast (ray, out hit, 1000)) {
						tapObject (hit.transform);
					}
				}
			}
        }

        if (Application.isEditor)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, 1000) && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
                {
                     tapObject(hit.transform);
                    
                }
            }
        }
    }
}

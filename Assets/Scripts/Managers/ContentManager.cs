﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContentManager : MonoBehaviour {

   public Content currContent;
    public string currContentID;

    void OnEnable() {
        WeARCloudRecoTrackableEventHandler.TargetFind += TargetFind;
        WeARCloudRecoTrackableEventHandler.TargetLost += TargetLost;
        ContentLoader.LoadFinishOk += ShowContent;
        // tests
        Tests.ServerRequest += TargetFind;
    }

    void Start() {
        CacheKeeper.cache = CacheKeeper.LoadCacheFile();
    }


    void TargetFind(string targetID) {

        if (CacheKeeper.cache.ContainsKey(targetID)) {
            Debug.Log("Load from cache");
            ShowContent(targetID);
        }
        else GetComponent<ContentLoader>().LoadContentFromServer(targetID);
    }

    public void ShowContent(string id) {
        currContentID = id;
        currContent = CacheKeeper.cache[id];
        try {
            currContent.ShowContent(id);
        }
        catch (Exception e) {
            Debug.Log(e.Message);
            CacheKeeper.cache.Remove(id);
        }
    }

   private void TargetLost()
    {
        GameObject go = GameObject.Find(currContentID);
        if(go!=null) go.SetActive(false);
        GameObject go2 = GameObject.Find(currContentID+"but");
        if (go2 != null) go2.SetActive(false);
    }

   void OnDisable() {
        CacheKeeper.SaveCacheFile();
        WeARCloudRecoTrackableEventHandler.TargetFind -= TargetFind;
        WeARCloudRecoTrackableEventHandler.TargetLost -= TargetLost;
        ContentLoader.LoadFinishOk -= ShowContent;
        // tests
        Tests.ServerRequest -= TargetFind;
    }
}

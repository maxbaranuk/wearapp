﻿using UnityEngine;
using System.Collections;

public class ApplyShader : MonoBehaviour
{
    private Material[] thisMaterial;
    private string[] shaders;

    void Start()
    {
        Renderer[] meshRenderArray = GetComponentsInChildren<Renderer>();
        foreach (Renderer mr in meshRenderArray)
        {
            //                message("Found MeshRenderer " + mr.name);
            foreach (Material m in mr.sharedMaterials)
            {
				if (m != null) {
					var shaderName = m.shader.name;
					Debug.Log (shaderName);
					var newShader = Shader.Find (shaderName);
					if (newShader != null) m.shader = newShader;
				}
            }
        }
    }


}
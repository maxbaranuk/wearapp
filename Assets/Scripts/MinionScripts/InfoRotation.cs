﻿using UnityEngine;

public class InfoRotation : MonoBehaviour
{

    Transform objTr;

	void Start ()
    {
        objTr = GetComponent<Transform>();
    }
	
	void FixedUpdate ()
    {
        objTr.Rotate(Vector3.up, .5f);
	}
}

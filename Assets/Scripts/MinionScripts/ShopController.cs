﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopController : MonoBehaviour {

    int currState;
//    public GameObject changeButton;
    public GameObject[][] clothes;
    public GameObject[] defaultClothes;
    public GameObject[] sombreroClothes;
    public GameObject[] havaiClothes;
    public GameObject[] deerClothes;
    public GameObject[] gadyaClothes;
    public GameObject[] hatClothes;
    public GameObject[] homeworkerClothes;
    // Use this for initialization
    void Awake () {
        clothes = new GameObject[7][];
        clothes[0] = defaultClothes;
        clothes[1] = sombreroClothes;
        clothes[2] = havaiClothes;
        clothes[3] = deerClothes;
        clothes[4] = gadyaClothes;
        clothes[5] = hatClothes;
        clothes[6] = homeworkerClothes;

        //Transform ui = GameObject.Find("Canvas").transform.FindChild("MinionPanel");
        //changeButton = ui.FindChild("Change").gameObject;
        //changeButton.SetActive(false);
    }

    void OnEnable() {
//        changeButton.SetActive(true);
        UIManager.tapObject += Tap;
        currState = 0;
        foreach (GameObject g in clothes[currState])
        {
            g.SetActive(false);
        }
        currState = 1;
        foreach (GameObject g in clothes[currState])
        {
            g.SetActive(true);
        }
    }
	// Update is called once per frame
	void Tap (Transform tr) {
        if (tr.gameObject.tag == "Minion" && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject()) NextState();
    }

    public void NextState() {
        foreach (GameObject g in clothes[currState]) {
            g.SetActive(false);
        }
        currState++;
        if (currState > 6) currState = 0;
        foreach (GameObject g in clothes[currState])
        {
            g.SetActive(true);
        }
    }

    void OnDisable() {
        //       changeButton.SetActive(false);
        UIManager.tapObject -= Tap;
        foreach (GameObject g in clothes[currState])
        {
            g.SetActive(false);
        }
        currState = 0;
        foreach (GameObject g in clothes[currState])
        {
            g.SetActive(true);
        }
    }
}

﻿using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.Video;

public class MinionController : MonoBehaviour {

    Animator minionAnim;   
    public GameObject shopManager;
    public GameObject gameManager;
    public GameObject infoManager;
 //   GameObject mainLight;
 //   Quaternion lightRotation; 
//    public GameObject video;
    
    public GameObject[] Clothes;
    GameObject currManager;
    Transform ui;
   

    void Awake() {
        ui = GameObject.Find("Canvas").transform.FindChild("MinionPanel");

    }

    void OnEnable() {
        ui.GetComponent<RectTransform>().localScale = Vector3.one;
        UIManager.GameButtonClick += SetGameState;
        UIManager.ShopButtonClick += SetShopState;
        UIManager.InfoButtonClick += SetInfoState;
 //       mainLight = GameObject.Find("Directional light");
 //       lightRotation = mainLight.transform.localRotation;
//        mainLight.transform.localRotation = Quaternion.Euler(130,-30,0);
    }

    void Start () {
        minionAnim = GetComponentInChildren<Animator>();
        foreach (GameObject g in Clothes) g.SetActive(false);
        minionAnim.SetTrigger("Info");
		currManager = infoManager;
    }

    public void SetGameState() {
        StartCoroutine(StateChanger("ToGame"));
    }

    IEnumerator StateChanger(string nextState) {
        minionAnim.SetTrigger("Default");     
        yield return new WaitWhile(() => minionAnim.GetCurrentAnimatorStateInfo(0).IsName("Default"));
        currManager.SetActive(false);
//		yield return new WaitForSeconds (1f);
        switch (nextState) {
            case "ToGame":
                currManager = gameManager;
                break;
            case "Info":
                currManager = infoManager;
                break;
            case "Shop":
                currManager = shopManager;
                break;
        }
		yield return new WaitForEndOfFrame ();
        currManager.SetActive(true);
        minionAnim.SetTrigger(nextState);
    }

    public void SetInfoState()
    {
        StartCoroutine(StateChanger("Info"));
    }
    public void SetShopState()
    {
        StartCoroutine(StateChanger("Shop"));
    }

   

    //IEnumerator PlayVideo() {
    //    VideoPlayer videoPlayer = video.GetComponent<VideoPlayer>();
    //    AudioSource audioSource = video.GetComponent<AudioSource>();

    //    videoPlayer.EnableAudioTrack(0, true);
    //    videoPlayer.SetTargetAudioSource(0, audioSource);

    //    videoPlayer.url = "file:///" + path;
    //    videoPlayer.skipOnDrop = true;
    //    videoPlayer.Prepare();

    //    //Wait until video is prepared
    //    while (!videoPlayer.isPrepared)
    //    {
    //        Debug.Log("Preparing Video");
    //        yield return null;
    //    }
    //    Debug.Log("Done Preparing Video");
    //    videoPlayer.Play();
    //    audioSource.Play();
    //    videoPlayer.playOnAwake = true;
    //    audioSource.playOnAwake = true;
    //}

    

    void OnDisable() {
//        Destroy(video);
//        video.SetActive(false);
        ui.GetComponent<RectTransform>().localScale = Vector3.zero;
        UIManager.GameButtonClick -= SetGameState;
        UIManager.ShopButtonClick -= SetShopState;
        UIManager.InfoButtonClick -= SetInfoState;
//mainLight.transform.localRotation = lightRotation;
    }
   
}

﻿using UnityEngine;

public class Shadow : MonoBehaviour
{

    public GameObject body;
    Transform bodyTr;
    Transform objTr;

	void Start ()
    {
        objTr = GetComponent<Transform>();
        bodyTr = body.GetComponent<Transform>();
    }
	
	void Update ()
    {
        objTr.position = new Vector3(bodyTr.position.x, 0, bodyTr.position.z);
	}
}

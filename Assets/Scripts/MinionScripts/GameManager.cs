﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public int totalShots = 0;
    int goalShots = 0;
    int points;
    public GameObject arCam;
    Text scoretext;
//    public GameObject gamePanel;
    public bool isFinishGame;
    public float timeToFinish;
    public GameObject ball;
    public GameObject minion;
    public GameObject shadow;
    public GameObject platform;
    public GameObject basket;

    void Awake ()
    {
        Transform ui = GameObject.Find("Canvas").transform.FindChild("MinionPanel");
        scoretext = ui.FindChild("Score").GetComponent<Text>();
        arCam = Camera.main.gameObject;
        scoretext.gameObject.SetActive(false);
    }

    void OnEnable()
    {
        basket.SetActive(true);
        scoretext.gameObject.SetActive(true);
        Physics.gravity = -Vector3.forward * 9.8f;
        createNewBall();
    }

    void Update ()
    {
        
        if (timeToFinish < 0)
        {
            timeToFinish = 0;           
            isFinishGame = true;           
        }
    }

    public void addPoints()
    { 
        goalShots++;
        scoretext.text = "" + goalShots;
        points += 2;
    }

    public void createNewBall()
    {
        StartCoroutine(createBall());
    }

    IEnumerator createBall()
    {
        yield return new WaitForSeconds(0.4f);
        GameObject newBall = Instantiate(ball);
        newBall.gameObject.transform.parent = arCam.transform;
        newBall.GetComponent<Rigidbody>().isKinematic = true;
        newBall.transform.localPosition = new Vector3(0, -2f, 5f);
        newBall.GetComponent<Rigidbody>().useGravity = false;
    }

    void OnDisable()
    {
        basket.SetActive(false);
        //        gamePanel.SetActive(false);
        scoretext.gameObject.SetActive(false);
        GameObject[] objs = GameObject.FindGameObjectsWithTag("Banana");
        foreach (GameObject obj in objs)
            Destroy(obj);
        Physics.gravity = -Vector3.up * 9.8f;
    }
}

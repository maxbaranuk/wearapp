﻿using UnityEngine;
using System.Collections;

public class InfoPanel : MonoBehaviour 
{

//    EducationView manager;

    void OnEnable()
    {
//        InfoManager.infoClick += OpenPanel;
//        manager = GameObject.Find("MinionScene").GetComponentInChildren<EducationView>();
    }

   public void OpenPanel() 
	{
 //       if(name==gameObject.name)
        StartCoroutine(Open());
    }

	public void ClosePanel() 
	{
        StartCoroutine(Close(false));      
    }

    public void ShowVideo()
    {
        StartCoroutine(Close(true));
    }

	IEnumerator Open()
    {
        float alpha = 0;
        CanvasGroup canvGroup = GetComponent<CanvasGroup>();
        while (alpha < 1f)
        {
            alpha += .05f;
            canvGroup.alpha = alpha;
            yield return new WaitForSecondsRealtime(.02f);
		}
        alpha = 1;       
    }

	IEnumerator Close(bool video)
	{
        float alpha = 1;
        CanvasGroup canvGroup = GetComponent<CanvasGroup>();
        while (alpha > 0f)
        {
            alpha -= .05f;
            canvGroup.alpha = alpha;
            yield return new WaitForSecondsRealtime(.02f);
        }
        alpha = 0;
        gameObject.SetActive(false);
    }

    void OnDisable() {
//        InfoManager.infoClick -= OpenPanel;
        GetComponent<CanvasGroup>().alpha = 0;
    }
}

﻿using UnityEngine;

public class Ball : MonoBehaviour
{

//    GameObject cam;
    public bool isStay = true;
    Vector2 startPos;
    Vector2 endPos;
    Vector3 ballDir;
    GameManager manager;
    Transform tr;

    void Start ()
    {
//        cam = GameObject.Find("ARCamera");
        manager = GameObject.Find("GameManager").GetComponent<GameManager>();
        tr = GetComponent<Transform>();
    }
	
	void Update ()
    {
        if (transform.position.y < -15&& !isStay)
            Destroy(gameObject);     
        if (isStay)
            shotListener();       
    }

    void OnTriggerEnter(Collider otherCollider)
    {
        if (otherCollider.gameObject.tag == "winDetector")
        {
            manager.addPoints();
            Destroy(gameObject);
        }
    }

    void shotListener()
    {
        if (Input.touchCount > 0 & !manager.isFinishGame)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began) startPos = touch.position;
            
            if (touch.phase == TouchPhase.Moved)
            {
                float hor = 3 * touch.deltaPosition.x;
                float vert = 3 * touch.deltaPosition.y;
                Transform camTr = Camera.main.transform;
 //               cam.transform.Find("Camera");
                tr.RotateAround(tr.position, camTr.right * vert, 200 * Time.deltaTime);
                tr.RotateAround(tr.position, -camTr.up * hor, 200 * Time.deltaTime);
            }

            if (touch.phase == TouchPhase.Ended)
            {
                
                endPos = touch.position;
                Vector2 dir = endPos - startPos;
                if (dir.y > 50) makeShot(dir);              
            }
        }
    }

    void makeShot(Vector2 dir)
    {
        manager.totalShots++;
        isStay = false;

        Rigidbody rb = GetComponent<Rigidbody>();
        Transform camTr = Camera.main.transform;

        rb.isKinematic = false;
        rb.useGravity = true;
        ballDir = camTr.forward;
        ballDir.z = ballDir.z + 0.5f;
        tr.parent = null;
        rb.AddForce(ballDir * dir.y / 20, ForceMode.Impulse);
        rb.AddTorque(camTr.up * dir.y / 4);
        rb.AddTorque(-camTr.right * dir.x / 4);
        manager.createNewBall();
    }
}

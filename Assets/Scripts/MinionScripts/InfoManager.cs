﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class InfoManager : MonoBehaviour {

    public InfoPanel[] infoPanels;
    GameObject video;
    GameObject videoButton;
    string path;

    void Awake() {
        Transform panels = GameObject.Find("Canvas").transform.FindChild("MinionPanel").FindChild("InfoPanels");
        //        ui.FindChild("InfoPanels").gameObject.SetActive(true);     
        infoPanels = new InfoPanel[panels.childCount];
        for (int i = 0; i < infoPanels.Length; i++) {
            infoPanels[i] = panels.GetChild(i).GetComponent<InfoPanel>();
        }
        for (int i = 0; i < infoPanels.Length; i++)
        {
            infoPanels[i].gameObject.SetActive(false);
        }
        videoButton = infoPanels[infoPanels.Length - 1].gameObject;
        path = Path.Combine(Application.persistentDataPath, "Minion.mp4");
    }
	
    void OnEnable() {
            UIManager.tapObject += Tap;
            UIManager.MinionVideoButtonClick += ShowMinionVideo;
        if (!File.Exists(path)) StartCoroutine(LoadVideo(path));
    }

    void OnDisable() {
		if(video)
		video.SetActive (false);
        UIManager.tapObject -= Tap;
        UIManager.MinionVideoButtonClick -= ShowMinionVideo;
    }

    public void ShowMinionVideo()
    {
		if (video) {
			video.GetComponent<MediaPlayerCtrl> ().SeekTo (0);
			video.SetActive (true);
		}
		else {	
			video = Instantiate(Resources.Load<GameObject>("VideoPlane 1"), transform);
			video.GetComponent<MediaPlayerCtrl>().m_strFileName = "file://" + path;
			video.SetActive(true);
		}
  

        //        video.GetComponent<MediaPlayerCtrl>().Load("file://" + path);     
        //        video.GetComponent<MediaPlayerCtrl>().Play();
        //        StartCoroutine(PlayVideo());
    }

    IEnumerator LoadVideo(string path)
    {
        videoButton.SetActive(false);
		WWW media = new WWW("https://wear-app.s3.amazonaws.com/content-file-xiPTW2WJ0gri");
        yield return media;
        if (media.error != null)
        {
            Debug.Log("Fail Load");
        }
        else
        {
            FileStream ff = new FileStream(path, FileMode.Create);
            ff.Write(media.bytes, 0, media.bytesDownloaded);
            ff.Close();
            videoButton.SetActive(true);
        }
    }

    void OpenInfoPanel(string name)
    {
		if(video)
		video.SetActive (false);
        switch (name)
        {
            case "Info1":
                infoPanels[0].gameObject.SetActive(true);
                infoPanels[0].OpenPanel();
                break;
            case "Info2":
                infoPanels[1].gameObject.SetActive(true);
                infoPanels[1].OpenPanel();
                break;
            case "Info3":
                infoPanels[2].gameObject.SetActive(true);
                infoPanels[2].OpenPanel();
                break;
            case "Info4":
                infoPanels[3].gameObject.SetActive(true);
                infoPanels[3].OpenPanel();
                break;
            case "Video":
                infoPanels[4].gameObject.SetActive(true);
                infoPanels[4].OpenPanel();
                break;

        }
        Debug.Log(name);
    }

    void Tap(Transform tr) {
        if (tr.gameObject.tag == "Information" && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject()) OpenInfoPanel(tr.name);

    }
}

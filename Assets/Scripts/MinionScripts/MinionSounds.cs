﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinionSounds : MonoBehaviour {

    public AudioClip banana;
    public AudioClip bello;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PlaySound(string name) {
        if (name == "Banana") StartCoroutine(DelayedPlayBanana());
        else if(name == "Bello") StartCoroutine(DelayedPlayBello());
    }

    IEnumerator DelayedPlayBanana() {
        yield return new WaitForSeconds(1.3f);
        GetComponent<AudioSource>().PlayOneShot(banana);
    }

    IEnumerator DelayedPlayBello()
    {
        yield return new WaitForSeconds(1.0f);
        GetComponent<AudioSource>().PlayOneShot(bello);
    }
}

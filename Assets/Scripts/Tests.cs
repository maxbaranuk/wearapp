﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tests : MonoBehaviour {

    public delegate void TestRequest(string id);
    public static TestRequest ServerRequest;

    public void SendTestRequest(string id) {
        ServerRequest(id);
    }
}

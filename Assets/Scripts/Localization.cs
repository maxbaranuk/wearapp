﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

public class Localization : MonoBehaviour {

    string path = "";
    public static Dictionary<string, string> dictionary;
	// Use this for initialization
	void Awake () {
        dictionary = new Dictionary<string, string>();
        switch (Application.systemLanguage)
        {
            case SystemLanguage.English:
                path = "XML/stringsEn";
                break;
            case SystemLanguage.Russian:
                path = "XML/stringsRu";
                break;
            case SystemLanguage.Ukrainian:
                path = "XML/stringsUa";
                break;
            default:
                path = "XML/stringsEn";
                break;
        }
        SetLanguage(path);
    }

    private void SetLanguage(string path)
    {
        XmlDocument xmlDoc = new XmlDocument();
        TextAsset GameAsset = Resources.Load<TextAsset>(path);
        xmlDoc.LoadXml(GameAsset.text);
        XmlNodeList levelsList = xmlDoc.GetElementsByTagName("Language");
        XmlNodeList levelcontent = levelsList[0].ChildNodes;
        foreach (XmlNode levelInfo in levelcontent) {
            dictionary.Add(levelInfo.Name, levelInfo.InnerText);
        }
    }
    }

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

public class ContentLoader :MonoBehaviour{

    public delegate void LoadAction(string id);
    public delegate void LoadProgress(float progress);
    public static event LoadAction LoadStart;
    public static event LoadAction LoadFinishError;
    public static event LoadAction LoadFinishOk;
    public static event LoadProgress showProgress;
    string _serverLink = "http://wear.us-west-2.elasticbeanstalk.com/application/download/content/";
    string pathDir;
    Content currLoadContent;
    WWW content;
    static ContentLoader instance;
    bool loading;

    public static ContentLoader Instance
    {
        get { return instance;}
    }

    void Awake() {
        if (instance == null) instance = gameObject.GetComponent<ContentLoader>();
    }

    void OnEnable() {
        LoadFinishError += LoadError;
        WeARCloudRecoTrackableEventHandler.TargetLost += TargetLost;
        pathDir = Path.Combine(Application.persistentDataPath, "Cache");
    }

    void LoadError(string mess) {
        StopCoroutine("Loader");
    }

    void TargetLost() {
        StopAllCoroutines();
        if (loading) {
            loading = false;
            LoadFinishError("Stopped");
        } 
//        StopCoroutine("Loader");
    }

    public void LoadAssetContent(Content c) {
        StartCoroutine(AssetLoader(c));
    }
    public void LoadZipContent(Content c) {
        StartCoroutine(ZipLoader(c));
    }
    public void LoadEmptyContent(Content c) {
        CacheKeeper.cache.Add(c.id, c);
        LoadFinishOk("" + c.id);
    }
    public void LoadDefaultContent(Content c) {
        StartCoroutine(DefaultContentLoader(c));
    }
    public void LoadContentFromServer(string id)
    {
        StartCoroutine(Loader(id));
    }

    IEnumerator AssetLoader(Content c)
    {
        content = new WWW(c.path);
        while (!content.isDone)
        {
            showProgress(content.progress);
            yield return null;
        }
        if (content.error != null) LoadFinishError("WWW download had an error:" + content.error + "\n");
        else
        {
            SaveLoadedContent(content, c);
            LoadFinishOk("" + c.id);
        }
        loading = false;
		CacheKeeper.cache.Add(c.id, c);
    }

    IEnumerator ZipLoader(Content c) {
        content = new WWW(c.path);
        while (!content.isDone)
        {
            showProgress(content.progress);
            yield return null;
        }
        if (content.error != null) LoadFinishError("WWW download had an error:" + content.error + "\n");
        else
        {
            try
            {
                if (!Directory.Exists(pathDir))
                    Directory.CreateDirectory(pathDir);
                FileStream ff = new FileStream(Path.Combine(pathDir, c.id), FileMode.Create);
                ff.Write(content.bytes, 0, content.bytesDownloaded);
                ff.Close();

                CacheKeeper.cache.Add(c.id, c);

                ZipUtil.Unzip(Path.Combine(Path.Combine(Application.persistentDataPath, "Cache"), c.id), Path.Combine(Application.persistentDataPath, c.id));
                File.Delete(Path.Combine(pathDir, c.id));
                LoadFinishOk("" + c.id);
            }
            catch (Exception e)
            {
                LoadFinishError("Unzip error:" + e.Message + "\n");
            }

        }
        loading = false;
    }

    IEnumerator DefaultContentLoader(Content c) {
        content = new WWW(c.path);
        while (!content.isDone)
        {
            showProgress(content.progress);
            yield return null;
        }
        if (content.error != null) LoadFinishError("WWW download had an error:" + content.error + "\n");
        else
        {
            SaveLoadedContent(content, c);
            LoadFinishOk("" + c.id);
        }
        loading = false;
    } 

    IEnumerator Loader(string id) {
        loading = true;
        LoadStart("");
        yield return StartCoroutine(LoadData(id));   
        currLoadContent.LoadContent();
 //       yield return StartCoroutine(LoadContent(currLoadContent));
    }

    IEnumerator LoadData(string id) {

        UnityWebRequest www = UnityWebRequest.Get(_serverLink + id);
        yield return www.Send();
        if (www.isError) LoadFinishError("web request error");
        else
        {
            try
            {
                LoadAndParseJSON(www);
            }
            catch(Exception e) {
                LoadFinishError("Load error Handler  "+e.Message);
            }
        }
    }

    void LoadAndParseJSON(UnityWebRequest www) {
        JsonClass myObject = JsonUtility.FromJson<JsonClass>(www.downloadHandler.text);
        Debug.Log(www.downloadHandler.text);
        currLoadContent = null;
        if (myObject.mediaContents.Length > 0)
        {
            switch (myObject.mediaContents[0].mediaContentType)
            {
                case "VIDEO":
                    currLoadContent = new VideoContent(myObject);
                    break;
                case "AUDIO":
                    currLoadContent = new AudioContent(myObject);
                    break;
                case "IMAGE":
                    currLoadContent = new ImageContent(myObject);
                    break;
                case "MODEL_ZIP":
                    currLoadContent = new ZipContent(myObject);
                    break;
                case "IOS_MODEL":
                case "ANDROID_MODEL":
                    currLoadContent = new AssetContent(myObject);
                    break;
                default:
                    currLoadContent = new EmptyContent(myObject);
                    break;
            }
        }
        else {
            currLoadContent = new EmptyContent(myObject);
        }
       
    }

    //IEnumerator LoadContent(Content c) {
    //    content = null;
    //    while (!Caching.ready)
    //    yield return null;
    //    Debug.Log(c.GetType());
    //    Type t = c.GetType();
    //    Debug.Log(t.Name);

    //    switch (t.Name) {
    //        case "EmptyContent":
    //            CacheKeeper.cache.Add(c.id, c);
    //            LoadFinishOk("" + c.id);
    //            break;
    //        case "AssetContent":
    //            content = WWW.LoadFromCacheOrDownload(c.path, 0);
    //            while (!content.isDone)
    //            {
    //                showProgress(content.progress);
    //                yield return null;
    //                if (content.error != null) LoadFinishError("WWW download had an error:" + content.error + "\n");
    //                else
    //                {
    //                    CacheKeeper.cache.Add(c.id, c);
    //                    content.assetBundle.Unload(true);
    //                    LoadFinishOk("" + c.id);
    //                }
    //            }
    //            break;
    //        case "ZipContent":
    //            content = new WWW(c.path);
    //            while (!content.isDone)
    //            {
    //                showProgress(content.progress);
    //                yield return null;
    //            }
    //            if (content.error != null) LoadFinishError("WWW download had an error:" + content.error + "\n");
    //            else
    //            {
    //                try {
    //                    if (!Directory.Exists(pathDir))
    //                        Directory.CreateDirectory(pathDir);
    //                    FileStream ff = new FileStream(Path.Combine(pathDir, c.id), FileMode.Create);
    //                    ff.Write(content.bytes, 0, content.bytesDownloaded);
    //                    ff.Close();

    //                    CacheKeeper.cache.Add(c.id, c);

    //                    ZipUtil.Unzip(Path.Combine(Path.Combine(Application.persistentDataPath, "Cache"), c.id), Path.Combine(Application.persistentDataPath, c.id));
    //                    File.Delete(Path.Combine(pathDir, c.id));
    //                    LoadFinishOk("" + c.id);
    //                }
    //                catch (Exception e) {
    //                    LoadFinishError("Unzip error:" + e.Message+ "\n");
    //                }
                    
    //            }
    //            break;

    //        default:
    //            content = new WWW(c.path);
    //            while (!content.isDone)
    //            {
    //                showProgress(content.progress);
    //                yield return null;
    //            }
    //            if (content.error != null) LoadFinishError("WWW download had an error:" + content.error + "\n");
    //            else {
    //                SaveLoadedContent(content, c);
    //                LoadFinishOk("" + c.id);
    //            }
    //            break;
    //    }
    //}

    void SaveLoadedContent(WWW media, Content c) {
        Debug.Log("Saved id"+c.id);
        
        string descr = "";

        if (c is VideoContent) descr = ".mp4";

        if (media != null)
        {
            try
            {
                if (!Directory.Exists(pathDir))
                    Directory.CreateDirectory(pathDir);
                FileStream ff = new FileStream(Path.Combine(pathDir, c.id+descr), FileMode.Create);
                ff.Write(media.bytes, 0, media.bytesDownloaded);
                ff.Close();
                CacheKeeper.cache.Add(c.id, c);
//                LoadFinishOk(""+ c.id);
            }
            catch (Exception e){
                LoadFinishError("Save content Error - "+e.Message);
            }
        }
        else {
            LoadFinishError("Media - null");
        }
    }

    void OnDisable()
    {
        LoadFinishError -= LoadError;
        WeARCloudRecoTrackableEventHandler.TargetLost -= TargetLost;
    }
}

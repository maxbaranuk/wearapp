﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleStatesChanger : StateMachineBehaviour {

    public string currState;
//    OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        int idleState = Random.Range(0, 10);
        switch (idleState) {
            case 0:
                animator.SetTrigger("Idle1");
                break;
            case 1:
                animator.SetTrigger("Idle2");
                break;
            case 2:
            case 3:
            case 4:
                animator.SetTrigger("Idle3");
                break;
            case 5:
            case 6:
            case 7:
                animator.SetTrigger("Idle4");
                break;
            case 8:
            case 9:
                animator.SetTrigger("Idle5");
                break;
        }
        if(currState=="Info") 
        animator.SetTrigger("PrepareInfo");
        if (currState == "Shop")
        animator.SetTrigger("PrepareShop");
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}

﻿using UnityEngine;

public class HouseManager : MonoBehaviour
{
    [SerializeField]
    GameObject _houseButton;
    [SerializeField]
    GameObject _planButton;


	void OnEnable(){

		UIManager.tapObject += Cast;
	}

	void OnDisable(){

		UIManager.tapObject -= Cast;
	}

    public void ShowPlan()
    {
		GetComponent<Animator>().SetTrigger("Plan");
    }

    public void ShowHouse()
    {
		GetComponent<Animator>().SetTrigger("House");
    }

    void SetActive(GameObject go)
    {
        go.SetActive(!go.activeSelf ? true : false);
    }

	void Cast(Transform tr)
    {
		if (tr.name == _houseButton.name)
			ShowHouse();
		else if (tr.name == _planButton.name)
			ShowPlan();   
    }
}

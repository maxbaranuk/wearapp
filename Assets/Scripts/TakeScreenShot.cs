﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.UI;
using System.Runtime.InteropServices;

public class TakeScreenShot : MonoBehaviour 
{

    public string message;
    public GameObject canvas;
    bool _isProcessing = false;

	public void ButtonShare()
	{
       if (!_isProcessing) 
			StartCoroutine (ShareScreenshot());
	}

	public IEnumerator ShareScreenshot()
	{
		_isProcessing = true;
        canvas.SetActive(false);
        yield return new WaitForEndOfFrame ();

        Texture2D screenTexture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, true);
        screenTexture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        screenTexture.Apply();

        string date = System.DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss");

#if UNITY_ANDROID

        AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");

        AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

        var imageUrl = SaveImageToGallery(screenTexture, "screenshot_" + date, "", currentActivity);

        AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");

        AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

        intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));

        AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");

        AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", imageUrl);

        intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);

        intentObject.Call<AndroidJavaObject>("setType", "image/*");

        currentActivity.Call("startActivity", intentObject);


#elif UNITY_IOS
        byte[] dataToSave = screenTexture.EncodeToPNG ();
		string path = Application.persistentDataPath + "/wear"+ date + ".png";
		File.WriteAllBytes(path, dataToSave);
        CallSocialShareAdvanced("", "", "", path);
#else
		Debug.Log("No sharing set up for this platform.");
#endif


        _isProcessing = false;
        canvas.SetActive(true);
    }

#if UNITY_IOS
    public struct ConfigStruct
    {
        public string title;
        public string message;
    }

    [DllImport("__Internal")]
    private static extern void showAlertMessage(ref ConfigStruct conf);

    public struct SocialSharingStruct
    {
        public string text;
        public string url;
        public string image;
        public string subject;
    }

    [DllImport("__Internal")]
    private static extern void showSocialSharing(ref SocialSharingStruct conf);

    public static void CallSocialShare(string title, string message)
    {
        ConfigStruct conf = new ConfigStruct();
        conf.title = title;
        conf.message = message;
        showAlertMessage(ref conf);
    }

    public static void CallSocialShareAdvanced(string defaultTxt, string subject, string url, string img)
    {
        SocialSharingStruct conf = new SocialSharingStruct();
        conf.text = defaultTxt;
        conf.url = url;
        conf.image = img;
        conf.subject = subject;

        showSocialSharing(ref conf);
    }
#endif
    private const string MediaStoreImagesMediaClass = "android.provider.MediaStore$Images$Media";
    public static string SaveImageToGallery(Texture2D texture2D, string title, string description, AndroidJavaObject activity)
    {
        using (var mediaClass = new AndroidJavaClass(MediaStoreImagesMediaClass))
        {
            using (var cr = activity.Call<AndroidJavaObject>("getContentResolver"))
            {
                var image = Texture2DToAndroidBitmap(texture2D);
                var imageUrl = mediaClass.CallStatic<string>("insertImage", cr, image, title, description);
                return imageUrl;
            }
        }
    }

    public static AndroidJavaObject Texture2DToAndroidBitmap(Texture2D texture2D)
    {
        byte[] encoded = texture2D.EncodeToPNG();
        using (var bf = new AndroidJavaClass("android.graphics.BitmapFactory"))
        {
            return bf.CallStatic<AndroidJavaObject>("decodeByteArray", encoded, 0, encoded.Length);
        }
    }
}
﻿using System;

[Serializable]
public class TextClass
{
    public int id;
    public string textContent;
    public string textContentType;
}

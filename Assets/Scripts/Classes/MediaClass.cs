﻿using System;


[Serializable]
public class MediaClass
{
    public int id;
    public string contentFileName;
    public string contentFileType;
    public long contentFileSize;
    public string mediaContentType;
    public string contentUrl;
}
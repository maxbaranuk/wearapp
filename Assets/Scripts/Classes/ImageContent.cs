﻿using System;
using System.IO;
using UnityEngine;

[Serializable]
public class ImageContent : Content {

    public ImageContent(JsonClass data) : base(data)
    {
        path = data.mediaContents[0].contentUrl;
        size = data.mediaContents[0].contentFileSize;
    }

    public override void LoadContent()
    {
        ContentLoader.Instance.LoadDefaultContent(this);
    }

    public override void ShowContent(string id)
    {
        
        this.id = id;
        Debug.Log("Create Image");
        ContentCreator.Instance.CreateImageObject(id);
        if(textContent!=null) ContentCreator.Instance.CreateContactObject(id, textContent);

    }
}

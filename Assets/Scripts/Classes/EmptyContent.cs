﻿using System;

[Serializable]
public class EmptyContent : Content
{
    public EmptyContent(JsonClass data) : base(data)
    {
    }

    public override void LoadContent()
    {
        ContentLoader.Instance.LoadEmptyContent(this);
    }

    public override void ShowContent(string id)
    {
        if (textContent != null) ContentCreator.Instance.CreateContactObject(id, textContent);
    }
}

﻿using System;
using UnityEngine;

[Serializable]
public class ZipContent :Content{

    public ZipContent(JsonClass data) : base(data)
    {
        path = data.mediaContents[0].contentUrl;
        size = data.mediaContents[0].contentFileSize;
        contentFileName = data.mediaContents[0].contentFileName;
    }

    public override void LoadContent()
    {
        ContentLoader.Instance.LoadZipContent(this);
    }

    public override void ShowContent(string id)
    {
        this.id = id;
        Debug.Log("Create Zip");
        ContentCreator.Instance.CreateZipObject(id);
        if (textContent != null) ContentCreator.Instance.CreateContactObject(id, textContent);
    }
}

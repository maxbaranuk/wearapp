﻿
using System;

[Serializable]
public abstract class Content :IShowable
{
    public string id;
    public long size;
    public string path;
    public string contentFileName;
    public TextClass[] textContent;
    protected JsonClass data;
    public abstract void ShowContent(string id);
    public abstract void LoadContent();

    public Content(JsonClass data) {
        this.data = data;
        id = data.vuforiaId;
        if (data.textContents.Length > 0) textContent = data.textContents;
    }

    //[Serializable]
    //public enum ContentType { Video, Zip, AssetBundle, Image, Audio, Empty }
    //public ContentType contentType;

    //public abstract void ShowContent(string id);
}

﻿using System;
using UnityEngine;

[Serializable]
public class AudioContent : Content
{
    public AudioContent(JsonClass data) : base(data)
    {
        path = data.mediaContents[0].contentUrl;
        size = data.mediaContents[0].contentFileSize;
    }

    public override void LoadContent()
    {
        ContentLoader.Instance.LoadDefaultContent(this);
    }

    public override void ShowContent(string id)
    {
        this.id = id;
        Debug.Log("Create Audio");
        ContentCreator.Instance.CreateAudioObject(id, data.mediaContents[0].contentFileType);
        if (textContent != null) ContentCreator.Instance.CreateContactObject(id, textContent);
    }
}

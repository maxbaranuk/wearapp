﻿using System;
using UnityEngine;

[Serializable]
public class AssetContent : Content {

//    public WWW www;
    public AssetContent(JsonClass data) : base(data)
    {
        string type = "";
#if UNITY_IOS
        type = "IOS_MODEL";
#elif UNITY_ANDROID
        type = "ANDROID_MODEL";
#endif
        Debug.Log("type " + type);
        foreach (MediaClass mc in data.mediaContents) {
            if (mc.mediaContentType.Contains(type)) {
                path = mc.contentUrl;            
                size = mc.contentFileSize;
                Debug.Log("size " + size);
            } 
        }
    }

    public override void LoadContent()
    {
        ContentLoader.Instance.LoadAssetContent(this);
    }

    public override void ShowContent(string id)
    {
        Debug.Log("Create Asset ");
        ContentCreator.Instance.CreateAssetObject(id, path);
        if (textContent != null) ContentCreator.Instance.CreateContactObject(id, textContent);
    }
}

﻿using System;
using UnityEngine;

[Serializable]
public class VideoContent : Content
{
    public VideoContent(JsonClass data) : base(data)
    {
        path = data.mediaContents[0].contentUrl;
        size = data.mediaContents[0].contentFileSize;
    }

    public override void LoadContent()
    {
        ContentLoader.Instance.LoadDefaultContent(this);
    }

    public override void ShowContent(string id)
    {
        this.id = id;
        Debug.Log("Create Video");
        ContentCreator.Instance.CreateVideoObject(id);
        if (textContent != null) ContentCreator.Instance.CreateContactObject(id, textContent);
    }
}

﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class EyeScript : MonoBehaviour {
    public enum EventState {
        Event_On,
        Event_Off
    }

    public EventState eventState=EventState.Event_On;

    [SerializeField]
    float _startEventTime;
    [SerializeField]
    float _endEventTime;
    [SerializeField]
    float _hohoTime;
    [SerializeField]
    float _merryXmasTime;
    [SerializeField]
    Transform _cameraTrans;
    [SerializeField]
    Transform _secondEye;
    [SerializeField]
    Transform _eyeTrans;
    [SerializeField]
    Animator _animcontroller;
   // [SerializeField]
    AnimationClip _clip;
    [SerializeField]
    AudioSource _audiosource;
    [SerializeField]
    AudioClip _hoHoHo;
    [SerializeField]
    AudioClip _merryXmas;

    bool _isEyesFollow=true;

   
    void OnEnable() {
        _isEyesFollow = true;
    }
	// Use this for initialization
	void Start () {
        // _audiosource = GetComponent<AudioSource>();
        _cameraTrans = Camera.main.transform;
        if (eventState==EventState.Event_On) {
            AnimationEvent startAnimation;
            startAnimation = new AnimationEvent();
            startAnimation.time = _startEventTime;
            startAnimation.functionName = "StartAnimation";
            AnimationEvent endAnimation;
            endAnimation = new AnimationEvent();
            endAnimation.time = _endEventTime;
            endAnimation.functionName = "EndAnimation";

            _clip = _animcontroller.runtimeAnimatorController.animationClips[0];
            _clip.AddEvent(startAnimation);
            _clip.AddEvent(endAnimation);
        }

    }
	
	// Update is called once per frame
	void Update () {
        if (_isEyesFollow == true) {
            //UnityEngine.Debug.Log(_animation["Take 001"].normalizedTime);
            _eyeTrans.LookAt(_cameraTrans);


            _eyeTrans.localEulerAngles = new Vector3(ClampAngle(_eyeTrans.localEulerAngles.x, 320f, 10f),
                                                     ClampAngle(_eyeTrans.localEulerAngles.y, 320f, 60f),
                                                     ClampAngle(_eyeTrans.localEulerAngles.z, 0, 360f));
            //UnityEngine.Debug.Log(_eyeTrans.localEulerAngles);
            _secondEye.rotation = _eyeTrans.rotation;
        }
 
	}


    public void OnTrackingFound() {
        _audiosource.mute = false;
    }

    public void OnTrackingLost() {
        _audiosource.mute = true;
    }

    static float ClampAngle(float angle,float angleFrom,float angleTo) {
        if (angle <360 && angle>180 && angle<=angleFrom) {
            return angleFrom;
        }
        if (angle < 180 && angle >= angleTo) {
            return angleTo;
        }
        else {
            return angle;
        }
       

    }


    public void StartAnimation() {
        StartCoroutine(PlayHoHo());
       // StartCoroutine(PlayXmas());
        _isEyesFollow = false;
        _eyeTrans.rotation = Quaternion.identity;
        _secondEye.rotation = _eyeTrans.rotation;
        UnityEngine.Debug.Log("StartAnimation");
    }
    public void EndAnimation() {
        _isEyesFollow = true;
        UnityEngine.Debug.Log("EndAnimation");
    }



    IEnumerator PlayHoHo() {
        yield return new WaitForSeconds(_hohoTime);
        _audiosource.PlayOneShot(_hoHoHo);
    }

    IEnumerator PlayXmas() {
        yield return new WaitForSeconds(_merryXmasTime);
        _audiosource.PlayOneShot(_merryXmas);
    }
}

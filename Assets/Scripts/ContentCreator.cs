﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Video;

public class ContentCreator : MonoBehaviour {

    public delegate void CreateAction(string id);
    public static event CreateAction message;
    public static event CreateAction error;
    public static event CreateAction createStart;
    public static event CreateAction createFinish;
    static ContentCreator instance;
    string pathDir = "";
    Dictionary<string, GameObject> objectsOnScene;

    public static ContentCreator Instance {
        get
        {
            return instance;
        }
    }

    void Awake() {
       if(instance==null)  instance = gameObject.GetComponent<ContentCreator>();
        objectsOnScene = new Dictionary<string, GameObject>();
        pathDir = Path.Combine(Application.persistentDataPath, "Cache");
    }

    public void CreateImageObject(string id) {
        if (objectsOnScene.ContainsKey(id)) objectsOnScene[id].SetActive(true);
        else StartCoroutine(ImageCreator(id));
    }

    public void CreateVideoObject(string id)
    {
        if (objectsOnScene.ContainsKey(id)) objectsOnScene[id].SetActive(true);
        else StartCoroutine(EasyVideoCreator(id));
    }

    public void CreateAssetObject(string id, string path) {
        if (objectsOnScene.ContainsKey(id)) objectsOnScene[id].SetActive(true);
        else StartCoroutine(AssetCreator(id, path));
    }

    public void CreateZipObject(string id)
    {
        if (objectsOnScene.ContainsKey(id)) objectsOnScene[id].SetActive(true);
        else StartCoroutine(ZipCreator(id));
    }

    public void CreateAudioObject(string id, string type)
    {
        if (objectsOnScene.ContainsKey(id)) objectsOnScene[id].SetActive(true);
        else StartCoroutine(AudioCreator(id, type));
    }

    public void CreateContactObject(string id, TextClass[] data) {
        if (objectsOnScene.ContainsKey(id+"but")) objectsOnScene[id + "but"].SetActive(true);
        else StartCoroutine(ContactCreator(id, data));
    }


    IEnumerator AssetCreator(string id, string path) {
        message("Start Creating\n");
        createStart("");
        GameObject go;

        var bundleLoadRequest = AssetBundle.LoadFromFileAsync(Path.Combine(pathDir, id));
        yield return bundleLoadRequest;

        var myLoadedAssetBundle = bundleLoadRequest.assetBundle;
        if (myLoadedAssetBundle == null)
        {
            Debug.Log("Failed to load AssetBundle!");

            yield break;
        }

 //       var assetLoadRequest = myLoadedAssetBundle.LoadAssetAsync<GameObject>("MyObject");


        //WWW content = WWW.LoadFromCacheOrDownload(path, 0);
        //yield return content;
        //AssetBundle bundle = content.assetBundle;
 //           message(bundle.name+"\n");
 //           Debug.Log(bundle.name);
            GameObject prefab = myLoadedAssetBundle.LoadAllAssets<GameObject>()[0];

            go = Instantiate(prefab);
            go.name = id;
            message("Create complete "+ id + "\n");

            Renderer[] meshRenderArray = go.GetComponentsInChildren<Renderer>();
            foreach (Renderer mr in meshRenderArray)
            {
                foreach (Material m in mr.sharedMaterials)
                {
				if (m != null) {
					var shaderName = m.shader.name;
					Debug.Log (shaderName);
					var newShader = Shader.Find (shaderName);
					if (newShader != null) m.shader = newShader;

				}
                }
            }
            objectsOnScene.Add(id, go);
            myLoadedAssetBundle.Unload(false);
            createFinish("");
    }

    IEnumerator ContactCreator(string id, TextClass[] data) {
        createStart("");
        yield return new WaitForEndOfFrame();
        try
        {
            GameObject root = new GameObject();
            root.name = id+"but";
            List<GameObject> buttons = new List<GameObject>();
            GameObject butPrefab = Resources.Load<GameObject>("icon");
            Material phoneMat = Resources.Load<Material>("Materials/3d_button_phone");
            Material smsMat = Resources.Load<Material>("Materials/3d_button_message");
            Material mailMat = Resources.Load<Material>("Materials/3d_button_mail");
            Material linkMat = Resources.Load<Material>("Materials/3d_button_site");
            for (int i = 0; i < data.Length; i++)
            {
                if (data[i].textContentType.Equals("PHONE"))
                {
                    GameObject g = Instantiate(butPrefab, root.transform);
                    g.GetComponent<Renderer>().material = phoneMat;
                    g.name = "PHONE";
                    g.GetComponent<ContactIcon>().textContent = data[i].textContent;
                    buttons.Add(g);
                }
                if (data[i].textContentType.Equals("SMS"))
                {
                    GameObject g = Instantiate(butPrefab, root.transform);
                    g.name = "SMS";
                    g.GetComponent<ContactIcon>().textContent = data[i].textContent;
                    g.GetComponent<Renderer>().material = smsMat;
                    buttons.Add(g);
                }
                if (data[i].textContentType.Equals("EMAIL"))
                {
                    GameObject g = Instantiate(butPrefab, root.transform);
                    g.name = "EMAIL";
                    g.GetComponent<ContactIcon>().textContent = data[i].textContent;
                    g.GetComponent<Renderer>().material = mailMat;
                    buttons.Add(g);
                }
                if (data[i].textContentType.Equals("LINK"))
                {
                    GameObject g = Instantiate(butPrefab, root.transform);
                    g.name = "LINK";
                    g.GetComponent<ContactIcon>().textContent = data[i].textContent;
                    g.GetComponent<Renderer>().material = linkMat;
                    buttons.Add(g);
                }
            }

            int disp;
            if (buttons.Count % 2 == 0) disp = -2;
            else disp = -4;
            for (int i = 0; i < buttons.Count; i++) buttons[i].transform.position = new Vector3(disp * buttons.Count / 2 + 4 * i, 0, -7);
            objectsOnScene.Add(id+"but", root);
        }
        catch (Exception e)
        {
            error(e.Message);
        }
        createFinish("");
    }

    IEnumerator AudioCreator(string id, string type) {
        createStart("");
        yield return new WaitForEndOfFrame();
        WWW www = new WWW("file:///" + Path.Combine(pathDir, id));
        yield return www;
        try {
            GameObject go = Instantiate(Resources.Load("Sound") as GameObject);
            go.name = id;          
            AudioSource source = go.GetComponent<AudioSource>();
            if(type.Contains("audio/wav"))
                source.clip = www.GetAudioClip(false, false, AudioType.WAV);
            else if(type.Contains("audio/mpeg"))
                source.clip = www.GetAudioClip(false, false, AudioType.MPEG);
            source.Play();
            objectsOnScene.Add(id, go);
            createFinish("");
        }
        catch (Exception e)
        {
            error(e.Message);
        }

        
    }

    IEnumerator ZipCreator(string id) {
        createStart("");
        yield return new WaitForEndOfFrame();

            try
        {
            GameObject go;
 //           fileName = fileName.Replace(".zip", "");
 //           ZipUtil.Unzip(Path.Combine(pathDir, id), Path.Combine(Application.persistentDataPath, id));
            go = OBJLoader.LoadOBJFile(Directory.GetFiles(Path.Combine(Application.persistentDataPath, id), "*.obj", SearchOption.AllDirectories)[0]);
			if(go!=null){
            go.name = id;
            go.AddComponent<ItemScale>();
            Transform goTransform = go.GetComponent<Transform>();
            goTransform.localPosition = new Vector3(0, .1f, 0);
            goTransform.localEulerAngles = Vector3.zero;
            objectsOnScene.Add(id, go);
			}
 //           Directory.Delete(Path.Combine(Application.persistentDataPath, fileName), true);
        }
        catch (Exception e)
        {
            error(" Zip "+e.Message);
        }
       
        createFinish("");
    }

    IEnumerator ImageCreator(string id) {
        
        WWW media = new WWW("file:///" + Path.Combine(pathDir, id));
        yield return media;
        try
        {
            GameObject go = Instantiate(Resources.Load("Image") as GameObject);
        go.name = id;
        Texture2D texture2D = media.texture;
        go.GetComponent<Renderer>().material.mainTexture = texture2D;
        float coef = texture2D.width > texture2D.height ? texture2D.width : texture2D.height;
        go.transform.localScale = new Vector3(texture2D.width / coef, 1, texture2D.height / coef);
        objectsOnScene.Add(id, go);
        }
        catch (Exception e)
        {
            error(e.Message);
        }
    }

    IEnumerator EasyVideoCreator(string id) {

        GameObject go = Instantiate(Resources.Load<GameObject>("VideoPlane"));
        go.name = id;
//        go.GetComponent<MediaPlayerCtrl>().Load("file://" + Path.Combine(pathDir, id+".mp4"));
        go.GetComponent<MediaPlayerCtrl>().m_strFileName = "file://" + Path.Combine(pathDir, id + ".mp4");
        go.gameObject.SetActive(true);
        objectsOnScene.Add(id, go);
        //        go.GetComponent<MediaPlayerCtrl>().Play();
        yield return new WaitForEndOfFrame();
    }

    IEnumerator VideoCreator(string id)
    {     
        try
        {
            GameObject go = Instantiate(Resources.Load("Video") as GameObject);
            go.name = id;
            go.SetActive(true);
            StartCoroutine(PlayVideo(go, "file:///" + Path.Combine(pathDir, id)));
            objectsOnScene.Add(id, go);
        } catch(Exception e) {
            error(e.Message);
          }
        yield return null;
    }

    IEnumerator PlayVideo(GameObject video, string path)
    {
        VideoPlayer videoPlayer = video.GetComponent<VideoPlayer>();
        AudioSource audioSource = video.GetComponent<AudioSource>();    

        videoPlayer.EnableAudioTrack(0, true);
        videoPlayer.SetTargetAudioSource(0, audioSource);

        videoPlayer.url = path + ".mp4";
        videoPlayer.skipOnDrop = true;
        videoPlayer.Prepare();

        float time = 0;
        //Wait until video is prepared
        while (!videoPlayer.isPrepared && time < 2)
        {
            Debug.Log("Preparing Video");
            time += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        //Debug.Log("Done Preparing Video");
       
        videoPlayer.Play();
        audioSource.Play();
        videoPlayer.playOnAwake = true;
        audioSource.playOnAwake = true;
    }
}

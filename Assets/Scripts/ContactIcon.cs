﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContactIcon : MonoBehaviour {

    public string textContent = "";

	void OnEnable () {
        UIManager.tapObject += Click;
	}

	void OnDisable () {
        UIManager.tapObject -= Click;
    }

    void Click(Transform tr) {
        switch (tr.gameObject.name) {
            case "PHONE":
                Application.OpenURL("tel://"+textContent);
                break;
            case "SMS":
                Application.OpenURL("sms://"+textContent+";body=loremipsum");
                break;
            case "EMAIL":
                string email = textContent;
                string body = MyEscapeURL("Please Enter your message here\n\n\n\n" +
                 "________");
                Application.OpenURL("mailto:" + email + "&body=" + body);
                break;
            case "LINK":
                Application.OpenURL(textContent);
                break;
        }
    }

    string MyEscapeURL(string url)
    {
        return WWW.EscapeURL(url).Replace("+", "%20");
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EggShattered : MonoBehaviour {

	Vector3 [] positions;
	Quaternion [] rotations;
	// Use this for initialization
	void Awake () {
		positions = new Vector3[transform.childCount];
		rotations = new Quaternion[transform.childCount];
		for (int i = 0; i < positions.Length; i++) {
			positions [i] = transform.GetChild (i).position;
			rotations [i] = transform.GetChild (i).rotation;
		}
	}

	void OnEnable(){
		for (int i = 0; i < positions.Length; i++) {
			transform.GetChild (i).position = positions [i];
			transform.GetChild (i).rotation = rotations [i];
		}
	}

}

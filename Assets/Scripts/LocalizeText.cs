﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocalizeText : MonoBehaviour {

    public string key;
	// Use this for initialization
	void Start () {
        GetComponent<Text>().text = Localization.dictionary[key];
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

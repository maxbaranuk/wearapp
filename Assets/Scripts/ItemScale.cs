﻿using UnityEngine;

public class ItemScale : MonoBehaviour
{

    long scaleCounter = 0;
    float rotateFactor = 250;
//    float touchStartAngle;
//    float startItemAngle;
//    Transform objTr;

    //void Start ()
    //{
    //    objTr = GetComponent<Transform>();
    //}
	
	void Update ()
    {
        if (Input.touchCount == 1) RotateItem();
        if (Input.touchCount == 2) resizeItem();
       
    }

    void resizeItem()
    {
        Touch touchZero = Input.GetTouch(0);
        Touch touchOne = Input.GetTouch(1);
        if ((touchZero.phase == TouchPhase.Moved | touchOne.phase == TouchPhase.Moved))
        {
            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;
            float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;
			deltaMagnitudeDiff = deltaMagnitudeDiff * 960 / Screen.height;
            if (scaleCounter % 2 == 0)
            {
				transform.localScale = new Vector3(transform.localScale.x*(1 - deltaMagnitudeDiff * .02f), transform.localScale.y *(1 - deltaMagnitudeDiff * .02f), transform.localScale.z *(1 - deltaMagnitudeDiff * .02f));
                if (transform.localScale.x < .1f | transform.localScale.y < .1f | transform.localScale.z < .1f)
                    transform.localScale = new Vector3(.1f, .1f, .1f);
            }
            scaleCounter++;
        }
    }

    void RotateItem()
    {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Moved)
            {
            float hor = touch.deltaPosition.x * rotateFactor / Screen.height;
            transform.rotation = Quaternion.Euler(transform.eulerAngles.x, transform.eulerAngles.y - hor, transform.eulerAngles.z);
        }

           
        }
}

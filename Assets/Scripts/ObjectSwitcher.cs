﻿using UnityEngine;

public class ObjectSwitcher : MonoBehaviour {


    GameObject [] items;
    Transform ui;
    int currentItem = 0;
    // Use this for initialization
    void Awake () {
        items = new GameObject[transform.childCount];
        for (int i = 0; i < items.Length; i++) {
            items[i] = transform.GetChild(i).gameObject;
        }
        foreach (GameObject g in items)
        {
            g.SetActive(false);
        }
        ui = GameObject.Find("Canvas").transform.FindChild("Switcher");
        items[0].SetActive(true);
    }

    void OnEnable() {
        ui.GetComponent<RectTransform>().localScale = Vector3.one;
        UIManager.NextButtonClick += Next;
        UIManager.PreviousButtonClick += Prev;
    }

    void OnDisable() {
        ui.GetComponent<RectTransform>().localScale = Vector3.zero;
        UIManager.NextButtonClick -= Next;
        UIManager.PreviousButtonClick -= Prev;
    }
    public void Prev()
    {
        items[currentItem].SetActive(false);
        currentItem--;
        if (currentItem < 0) currentItem = items.Length - 1;
        items[currentItem].SetActive(true);
    }

    public void Next()
    {
        items[currentItem].SetActive(false);
        currentItem++;
        if (currentItem == items.Length) currentItem = 0;
        items[currentItem].SetActive(true);
    }
}

﻿using UnityEngine;
using System.IO;
#if (UNITY_EDITOR)
using UnityEditor;
#endif

public class CreateAssetBundles : MonoBehaviour
{
    public static GameObject go;
#if (UNITY_EDITOR)
    [MenuItem("Assets/Build AssetBundles For IOS")]
    static void BuildAllAssetBundlesForIOS()
    {
        Directory.CreateDirectory("Assets/AssetBundles/IOS");
        BuildPipeline.BuildAssetBundles("Assets/AssetBundles/IOS", BuildAssetBundleOptions.None, BuildTarget.iOS);
    }

    [MenuItem("Assets/Build AssetBundles For Android")]
    static void BuildAllAssetBundlesForAndroid()
    {
        Directory.CreateDirectory("Assets/AssetBundles/Android");
        BuildPipeline.BuildAssetBundles("Assets/AssetBundles/Android", BuildAssetBundleOptions.None, BuildTarget.Android);
    }

    [MenuItem("Assets/Build AssetBundles Both")]
    static void BuildAllAssetBundlesBoth()
    {
        Directory.CreateDirectory("Assets/AssetBundles/IOS");
        BuildPipeline.BuildAssetBundles("Assets/AssetBundles/IOS", BuildAssetBundleOptions.None, BuildTarget.iOS);
        Directory.CreateDirectory("Assets/AssetBundles/IOS");
        BuildPipeline.BuildAssetBundles("Assets/AssetBundles/Android", BuildAssetBundleOptions.None, BuildTarget.Android);
    }

    [MenuItem("Assets/Get AssetBundle Names")]
    static void GetNames()
    {
        var names = AssetDatabase.GetAllAssetBundleNames();
        foreach (var name in names)
        {
            Debug.Log("AssetBundle name is : " + name);
        }
    }
#endif
}
